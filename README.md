# Le Nocturnal

Projet de site internet pour un hôtel-restaurant : "Le Nocturnal", en association avec un groupe d'élèves du Lycée des métiers de Bazeilles.
Le projet de 1ère année de ces étudiants en BTS Hôtellerie-Restauration était de trouver une idée pour un Hôtel-Restaurant dans un bâtiment de Charleville-Mézières (Ardennes).
Leur principale inspiration a été le concept parisien "Dans le noir", lors duquel une salle de restaurant est plongée dans l'obscurité afin de mieux apprécier les saveurs des plats.
Ainsi, la nuit devait se faire ressentir dans le site Internet. C'est pourquoi j'ai choisi une bannière représentant un ciel étoilé. De plus, leur établissement se voulant un établissement de luxe, j'ai choisi le blanc et noir afin de créer un design sobre.
Au final, un site WordPress s'est avéré plus simple et adapté à ce qu'ils voulaient. Il est disponible à cette adresse : [Le Nocturnal](https://wordpress.com/home/lenocturnal.wordpress.com)

Ci-dessous, quelques captures d'écran de l'acceuil et des pages du bar et du restaurant.

!["Capture d'écran de l'accueil du site"](/Presentation/ScreenshotAccueil.png)

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

!["Capture d'écran de la page du Bar"](/Presentation/ScreenshotBar.png)

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

!["Capture d'écran de la page du Restaurant"](/Presentation/ScreenshotMenu.png)